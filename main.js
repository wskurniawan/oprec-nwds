const express = require('express');
const path = require('path');

var app = express();

app.use('/public', express.static(__dirname + '/public'));

app.get('/', function(req, res, next){
   res.sendFile(path.resolve(__dirname + '/index.html'));
});

app.listen(process.env.PORT, function(){
   console.log('app ready di port ' + process.env.PORT);
});